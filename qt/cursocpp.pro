CONFIG(release, debug|release): TARGET = cursocpp
else:CONFIG(debug, debug|release): TARGET = cursocpp

QMAKE_CXXFLAGS += -std=c++17
CONFIG += C++17

INCLUDEPATH += \
    ../src \


HEADERS += \
# files
    ../src/day1.h \


SOURCES += \
    ../src/main.cpp \
# files
