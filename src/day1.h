#include <iostream>
#include <type_traits>
#include <vector>
#include <list>

/*
 * Ejercicio 1.
 * Escriba una función que sume dos valores de tipos arbitrarios y que emita un error de compilación
 * si alguno de los tipos no es un tipo aritmético.
 * La función debe poderse usar en contextos como el siguiente:
 *
 * void f() {
 *  auto x1 = sum(1, 2); // OK
 *  auto x2 = sum(1, 1.5); // OK
 *  auto x3 = sum(1, "hola"; // Error
 * }
*/

template <typename T, typename U>
auto sum(T a, U b)
{
    static_assert(std::is_arithmetic<T>::value && std::is_arithmetic<U>::value);
    return a + b;
}

namespace day1
{

void test_sum()
{
    std::cout << "Testing sum\n";
    std::cout << sum(1, 2) << std::endl; // OK
    std::cout << sum(1, 1.5) << std::endl << std::endl; // OK
    //sum(1, "hola"; // Error
}

}



/* Ejercicio 2.
 * Escriba una función que tome dos contenedores de secuencia arbitrarios y calcule su producto
 * escalar. La función debe devolver el resultado en un std::vector<double>.
 *
 * La función debe poderse usar de la siguiente forma:
 *
 * void f() {
 *  std::vector<double> v {1.0, 2.0, 3.0};
 *  std::list<double> w{0.5, 0.5, 0.5};
 *  auto t = scalar<std::vector<double>>(v,w);
 *  //...
 * }
 */

template <typename T, typename U>
std::vector<double> scalar(T container1, const U& container2)
{
    if (container1.size() != container2.size())
        return std::vector<double>();

    std::vector<double> ret{ std::begin(container1), std::end(container1) };
    int i(0);
    for (const auto& c : container2)
        ret[i++] *= c;

    return ret;
}

namespace day1
{

void test_scalar()
{
    std::cout << "Testing scalar\n";
    std::vector<double> v {1.0, 2.0, 3.0};
    std::list<double> w{0.5, 0.5, 0.5};
    for (const auto& x : scalar(w,v))
        std::cout << x << ", ";
    std::cout << "\n\n";
}

}



/* Ejercicio 3.
 * Escriba una clase fixed_vector para vectores de tamaño fijo con la siguiente interfaz:
 *
 * template <typename T>
 * class fixed_vector {
 *  public:
 *      explicit fixed_vector(int n);
 *      T & operator[](int n);
 *      T const & operator[](int n) const;
 *  private:
 *      int size_;
 *      T ∗ vec_;
 * };
 *
 * Modifique dicha clase para que pueda usarse con bucles-for basados en rango, de manera que el
 * siguiente código sea válido.
 *
 * int main() {
 *  std::cout << "Testing fixed_vector\n";
 *  fixed_vector<double> v(5);
 *  v[2] = 3.0;
 *  for (auto & x : v) {
 *      x++;
 *  }
 *  for (const auto & x : v) {
 *      std::cout << x << ", ";
 *  }
 * }
 *
 */

template <typename T>
class fixed_vector
{
public:
    explicit fixed_vector(int n) : _size(n), _vec(new T[n])
    {}

    ~fixed_vector()
    {
        delete[] _vec;
        _vec = nullptr;
    }

    T& operator[](int n)
    {
        if (n<0 || n>=_size)
            throw std::range_error("Out of range");
        return _vec[n];
    }

    const T& operator[](int n) const
    {
        if (n<0 || n>=_size)
            throw std::range_error("Out of range");

        return _vec[n];
    }

    T* begin() const { return &_vec[0]; }
    T* end() const { return &_vec[_size]; }

private:
    int _size;
    T* _vec;
};

namespace day1
{

void test_fixed_vector()
{
    std::cout << "Testing fixed_vector\n";
    fixed_vector<double> v(5);
    v[2] = 3.0;

    for (auto & x : v)
        x++;

    for (const auto& x : v)
        std::cout << x << ", ";
}

}
