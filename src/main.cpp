/* Ejercicios curso c++ 25-28/03/2019
 * Prof. Jose Daniel García
 * Campus Carlos III, Madrid
 */

#include "day1.h"

int main()
{
    {
        using namespace day1;
        test_sum();
        test_scalar();
        test_fixed_vector();
    }

    return 0;
}
