# build dir
REL_BUILD_DIR=$1

# build mode
MODE=$2
if [ -z "$MODE" ]
then
	echo "Invalid build mode."
	exit 1
fi
if [ $MODE != Debug ] && [ $MODE != Release ]
then
	echo "Invalid build mode."
	exit 1
fi

# work dir
WORK_DIR=$(pwd)

# script dir
SCRIPT_DIR=$WORKDIR$(dirname "$0")

# build dir
BUILD_DIR=$SCRIPT_DIR/$REL_BUILD_DIR
if [ $MODE = Debug ]
then
    BUILD_DIR=$BUILD_DIR/debug
else
    BUILD_DIR=$BUILD_DIR/release
fi
if [ ! -d "$BUILD_DIR" ]
then
    echo "Build directory not found."
    exit
fi

# remove files in build dir
rm -rf $BUILD_DIR/*
