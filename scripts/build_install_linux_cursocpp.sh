#!/bin/sh

# build directory (relative to script dir)
REL_BUILD_DIR=../build/cursocpp/

# source directory (relative to build dir)
REL_SOURCE_DIR=../../src/

# build mode
MODE=$1

# build and install
sh $(pwd)/$(dirname "$0")/build_install_linux.sh $REL_BUILD_DIR $REL_SOURCE_DIR $MODE
