#!/bin/sh

# build directory (relative to script dir)
REL_BUILD_DIR=$1

# source directory (relative to build dir)
REL_SOURCE_DIR=$2
	
# build mode
MODE=$3
if [ -z "$MODE" ]
then
	echo "Invalid build mode."
	exit 1
fi
if [ $MODE != Debug ] && [ $MODE != Release ]
then
	echo "Invalid build mode."
	exit 1
fi

# cores
NUM_PROC=$4
if [ "$NUM_PROC" -lt 1 ]
then
	NUM_PROC=1
fi
if [ -z "$NUM_PROC" ]
then
	NUM_PROC=1
fi

# work dir
WORK_DIR=$(pwd)

# script dir
SCRIPT_DIR=$WORKDIR$(dirname "$0")

# build dir
BUILD_DIR=$SCRIPT_DIR/$REL_BUILD_DIR
if [ $MODE = Debug ]
then
	BUILD_DIR=$BUILD_DIR/debug
else
	BUILD_DIR=$BUILD_DIR/release
fi
mkdir -p $BUILD_DIR

# check if source dir exists
if [ ! -d "$BUILD_DIR/../$REL_SOURCE_DIR" ]
then
	echo "Invalid source directory."
	exit 1
fi

# cd into dir
GotoDir() {
	cd $1
}
GotoDir $BUILD_DIR

# build and install
cmake -DCMAKE_BUILD_TYPE=$MODE ../$REL_SOURCE_DIR
make -j$NUM_PROC
make install
